#!/bin/sh

if [ "$(echo "${PHP_VERSION} >= 7.4" | bc)" -eq 1 ]; then
  docker-php-ext-configure gd --with-webp --with-jpeg=/usr/include/ >> /var/log/php-build.log
else
  docker-php-ext-configure gd --with-jpeg-dir=/usr/include/ >> /var/log/php-build.log
  docker-php-ext-configure zip --with-libzip=/usr/include/ >> /var/log/php-build.log
fi
