#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>

const char *get_app_name(const char *path) {
    const char *slash = strrchr(path, '/');
    if (slash) {
        return slash + 1;
    }
    return path;
}

int toggle_xdebug(const char *program) {
    pid_t pid;
    int status;

    switch ((pid = fork())) {
        case -1:
            perror("Failed to spawn process");
            exit(EXIT_FAILURE);
        case 0:
            execlp(program, program, "xdebug", NULL);
            perror("Failed to toggle xdebug.");
            exit(EXIT_FAILURE);
        default:
            do {
                waitpid(pid, &status, 0);
            } while (!WIFEXITED(status));
            return 0;
    }
}

long get_fpm_pid() {
    FILE *file;
    char *buffer = NULL;
    size_t len;
    ssize_t bytes_read;
    long pid = 1;

    if ((file = fopen("/var/run/php-fpm.pid", "r"))) {
        bytes_read = getdelim(&buffer, &len, '\0', file);
        if (bytes_read != -1) {
            pid = strtol(buffer, NULL, 10);
        }
    }
    return pid;
}

int main(int argc, char **argv) {
    const char *name = get_app_name(argv[0]);
    // Force execute uid on subprocesses.
    setuid(geteuid());
    if (0 == strcmp(name, "enable_xdebug")) {
        toggle_xdebug("/usr/local/bin/docker-php-ext-enable");
        printf("xdebug enabled\n");
    } else {
        // toggle_xdebug("/usr/local/bin/docker-php-ext-disable");
        remove("/usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini");
        printf("xdebug disabled\n");
    }
    kill(get_fpm_pid(), SIGUSR2);
    return 0;
}
