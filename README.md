# APQC PHP

This image is designed to support APQC's PHP projects bundling common
extensions for Drupal, Lumen and Laravel based projects.

Source of the images behind https://hub.docker.com/r/apqc/php

Documentation TBD
