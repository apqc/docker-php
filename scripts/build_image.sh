#!/bin/sh

## This script expects the following environment variables to be set:
if [ -z "$DIRECTORY" ]; then
	echo "Missing directory argument"
	exit 1
fi

if [ -z "$PHP_VERSION" ]; then
	echo "Missing version argument"
	exit 1
fi

if [ -z "$VARIANT" ]; then
	echo "Missing variant argument"
	exit 1
fi

tag="apqc/php:$PHP_VERSION-fpm-$VARIANT"
dev_tag="apqc/php:$PHP_VERSION-fpm-$VARIANT-dev"
docker build --pull -t $tag -f "$DIRECTORY/Dockerfile" $DIRECTORY
docker build --pull -t $dev_tag -f "$DIRECTORY/Dockerfile.dev" $DIRECTORY
docker push $tag
docker push $dev_tag
