#!/usr/bin/python3

import os
import pathlib
import re
import shutil
from shutil import copytree


def versions():
    ALPINE_3_15 = 'alpine3.15'
    ALPINE_3_16 = 'alpine3.16'
    ALPINE_3_17 = 'alpine3.17'
    ALPINE_3_18 = 'alpine3.18'
    ALPINE_3_19 = 'alpine3.19'
    ALPINE_3_20 = 'alpine3.20'
    ALPINE_3_21 = 'alpine3.21'
    return {
        '7.2': {
        },
        '7.3': {
        },
        '7.4': {
        },
        '8.0': {
        },
        '8.1': {
            ALPINE_3_20: [],
            ALPINE_3_21: [],
        },
        '8.2': {
            ALPINE_3_20: [],
            ALPINE_3_21: [],
        },
        '8.3': {
            ALPINE_3_20: [],
            ALPINE_3_21: [],
        },
        '8.4': {
            ALPINE_3_20: [],
            ALPINE_3_21: [],
        },
    }


def process_variant(php_version, alpine_version, data):
    directory = f"{php_version}/{alpine_version}/fpm"
    docker_file = f"{directory}/Dockerfile"
    pathlib.Path(directory).mkdir(parents=True, exist_ok=True)

    print('Building Dockerfile')
    replacements = {
        '%%PHP_TAG%%': f'{php_version}-fpm-{alpine_version}',
        '%%PHP_VERSION%%': f'{php_version}',
        '%%VARIANT%%': f'{alpine_version}',
    }
    with open(docker_file, 'wt') as file_out:
        replace_copy('Base/Dockerfile-alpine.template', file_out, replacements)

    with open(f"{docker_file}.dev", 'wt') as file_out:
        with open(f"Base/dev-Dockerfile-header") as fin:
            file_out.write(fin.read())
        replace_copy('Base/Dockerfile-alpine.template', file_out, replacements)
        with open(f"Base/dev-Dockerfile-block") as fin:
            file_out.write(fin.read())

    print('Syncing scripts and sources')
    pathlib.Path(f"{directory}/scripts").mkdir(parents=True, exist_ok=True)
    copytree('Base/scripts', f"{directory}/scripts", dirs_exist_ok=True)
    shutil.copy('Base/toggle_xdebug.c', directory)

    with open('./.gitlab-ci.yml', 'at') as file_out:
        replace_copy('Base/gitlab_build.yml', file_out, replacements)
    with open('./build_all.sh', 'at') as file_out:
        replace_copy('Base/build.sh', file_out, replacements)


def replace_copy(file_in, file_out, replacements):
    regex = re.compile('(%s)' % '|'.join(map(re.escape, replacements.keys())))
    with open(file_in, 'rt') as fin:
        for line in fin:
            file_out.write(
                regex.sub(lambda mo: replacements[mo.string[mo.start():mo.end()]], line)
            )


def main():
    # echo '#!/bin/sh' > build_all.sh
    # cp Base/gitlab_base.yml .gitlab-ci.yml
    shutil.copy('Base/gitlab_base.yml', '.gitlab-ci.yml')
    with open('build_all.sh', 'w+') as f:
        f.write("#!/bin/sh\n\n")
        f.write("export STORAGE_DRIVER=vfs\n")
        f.write("export BUILDAH_LAYERS=true\n\n")
    for php_version, alpine_versions in versions().items():
        shutil.rmtree(php_version, True)
        for alpine_version, X in alpine_versions.items():
            process_variant(php_version, alpine_version, X)


if __name__ == '__main__':
    main()
