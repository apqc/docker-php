#!/bin/sh

docker run \
    --rm \
    -v $PWD:/workspace:ro \
    -w /workspace \
    --privileged \
    -it \
    quay.io/containers/buildah:latest /bin/bash /workspace/build_all.sh
