FROM alpine:latest
RUN apk --no-cache add gcc musl-dev
COPY toggle_xdebug.c /
RUN gcc -O2 --static -o /toggle_xdebug /toggle_xdebug.c

FROM php:8.3-fpm-alpine3.21

LABEL maintainer="James Gilliland <jgilliland@apqc.org>"

ENV PHP_VERSION 8.3
COPY ./scripts/configure.sh /configure.sh

# Install php extension wrapper script to deal with pecl script deprecation.
COPY --from=docker.io/mlocati/php-extension-installer:latest /usr/bin/install-php-extensions /usr/bin/

RUN apk -U upgrade \
  && apk add --virtual .apqc-build-deps \
    $PHPIZE_DEPS \
    pcre-dev libjpeg-turbo-dev libpng-dev libwebp-dev gmp-dev yaml-dev libzip-dev linux-headers \
    bc \
    wget \
    sudo \
    libxml2-dev \
  && apk add \
    pcre libltdl libjpeg-turbo libpng libwebp gmp yaml libzip \
    less \
    mysql-client \
    msmtp \
    libxml2 \
  && touch /var/log/php-build.log \
    && /bin/sh /configure.sh \
    && docker-php-ext-enable opcache \
    && docker-php-ext-install mysqli pdo_mysql gd gmp zip bcmath calendar >> /var/log/php-build.log \
  && echo "Building APCU" \
    && install-php-extensions apcu >> /var/log/php-build.log \
    && docker-php-ext-enable apcu \
  && echo "Building PECL YAML" \
    && install-php-extensions yaml >> /var/log/php-build.log \
    && docker-php-ext-enable yaml \
  && echo "Building PECL Redis" \
    && install-php-extensions redis >> /var/log/php-build.log \
    && docker-php-ext-enable redis \
  && echo "Building Memcached" \
    && install-php-extensions memcached >> /var/log/php-build.log \
    && docker-php-ext-enable memcached \
  && echo "Building SOAP" \
    && docker-php-ext-install soap >> /var/log/php-build.log \
    && rm ${PHP_INI_DIR}/conf.d/docker-php-ext-soap.ini \
  && echo "Installing NewRelic" \
    && if [ "$(printf '%s\n' "8.1" "${PHP_VERSION}" | sort -V | head -n1)" = "8.2" ]; then \
       echo "Skipping NewRelic build. Unsupported." \
    ; else \
      mkdir -p /opt/newrelic \
      && rm -rf /etc/dpkg \
      && cd /opt/newrelic \
      && echo "  downloading..." \
      && wget -r -nd --no-parent -Alinux-musl.tar.gz https://download.newrelic.com/php_agent/release/ >/dev/null 2>&1 \
      && tar zxf *linux-musl.tar.gz \
      && cd `find /opt/newrelic/ -type d -maxdepth 1 -mindepth 1` \
      && echo "  installing..." \
      && NR_INSTALL_SILENT=true /bin/sh newrelic-install install \
      && echo "  cleanup up..." \
      && rm /opt/newrelic/*.tar.gz \
      && find /opt/newrelic/newrelic*/agent/ -type f | grep -v $(readlink $(find /usr/local/lib -name newrelic.so)) | xargs -r rm \
      && cp /tmp/nrinstall* /var/log/ \
      && gzip /var/log/nrinstall* \
    ; fi \
  && echo "Building xdebug" \
    && pecl install xdebug | tee /var/log/php-build.log \
    && rm -f ${PHP_INI_DIR}/conf.d/docker-php-ext-xdebug.ini \
  && cd /var/log/ \
    && gzip php-build.log \
  && apk del .apqc-build-deps \
  && rm -rf /tmp/* \
  && rm -rf /var/cache/apk/*


RUN mkdir -p /root/.ssh \
  && touch /usr/local/etc/php/conf.d/newrelic.ini \
  && sed -i "3 a if [ \"\${NR_LICENSE_KEY}x\" != \"x\" ]; then sed -i -e 's/REPLACE_WITH_REAL_KEY/\${NR_LICENSE_KEY}/g' /usr/local/etc/php/conf.d/newrelic.ini; else sed -i -e 's/;newrelic.enabled = true/newrelic.enabled = false/g' /usr/local/etc/php/conf.d/newrelic.ini; fi" /usr/local/bin/docker-php-entrypoint \
  && sed -i "3 a if [ \"\${NR_DISTRIBUTED}x\" != \"x\" ]; then sed -i -e \"s/;newrelic.distributed_tracing_enabled = false/newrelic.distributed_tracing_enabled = \${NR_DISTRIBUTED}/g\" /usr/local/etc/php/conf.d/newrelic.ini; fi" /usr/local/bin/docker-php-entrypoint

# This seems broken atm. Its better to run without health checks than with a broken one.
#RUN apk add --no-cache fcgi
#HEALTHCHECK --interval=10s --timeout=3s \
#  CMD SCRIPT_NAME=/ping SCRIPT_FILENAME=/ping REQUEST_METHOD=GET cgi-fcgi -bind -connect 127.0.0.1:9000 || exit 1
RUN apk add --no-cache bash shadow git patch sudo vim \
  && echo "Installing composer" \
    && php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
    && wget -q https://composer.github.io/installer.sig \
    && php -r "if (hash_file('SHA384', 'composer-setup.php') === file_get_contents('installer.sig') ) { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;" \
    && php composer-setup.php --2 --install-dir=/usr/local/bin --filename=composer \
    && php -r "unlink('composer-setup.php');" \
    && rm installer.sig \
  && echo "Installing extra dev applications" \
    && apk --no-cache add yarn npm python3 make g++ \
    && npm install --global gulp-cli \
  && echo "Installing Process Control" \
    && docker-php-ext-install pcntl \
  && echo "[global]" > /usr/local/etc/php-fpm.d/zzz-apqc.conf \
  && echo "pid = /var/run/php-fpm.pid" >> /usr/local/etc/php-fpm.d/zzz-apqc.conf


COPY --from=0 /toggle_xdebug /usr/bin/toggle_xdebug
RUN ln -sf /usr/bin/toggle_xdebug /usr/bin/enable_xdebug \
  && ln -sf /usr/bin/toggle_xdebug /usr/bin/disable_xdebug \
  && chown root:root /usr/bin/toggle_xdebug \
  && chmod 4555 /usr/bin/toggle_xdebug
