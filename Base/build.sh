###############################################################################
###############################################################################
###############################################################################
DIRECTORY="%%PHP_VERSION%%/%%VARIANT%%/fpm"
PHP_VERSION="%%PHP_VERSION%%"
VARIANT="%%VARIANT%%"
CI_REGISTRY_IMAGE=${CI_REGISTRY_IMAGE:-registry.gitlab.com/apqc/docker-php}
LOCAL_VERSION="${PHP_VERSION}-fpm-${VARIANT}"

echo Building apqc/php:${LOCAL_VERSION}
buildah manifest create apqc-php-${LOCAL_VERSION}
buildah bud \
      --tag "${CI_REGISTRY_IMAGE}:${LOCAL_VERSION}" \
      --manifest apqc-php-${LOCAL_VERSION} \
      --arch amd64 \
      --file $DIRECTORY/Dockerfile \
      $DIRECTORY
#buildah bud \
#      --tag "${CI_REGISTRY_IMAGE}:${LOCAL_VERSION}" \
#      --manifest apqc-php-${LOCAL_VERSION} \
#      --arch arm64 \
#      --file $DIRECTORY/Dockerfile \
#      $DIRECTORY
echo Building apqc/php:${LOCAL_VERSION}-dev
buildah manifest create apqc-php-${LOCAL_VERSION}-dev
buildah bud \
      --tag "${CI_REGISTRY_IMAGE}:${LOCAL_VERSION}-dev" \
      --manifest apqc-php-${LOCAL_VERSION}-dev \
      --arch amd64 \
      --file $DIRECTORY/Dockerfile.dev \
      $DIRECTORY
#buildah bud \
#      --tag "${CI_REGISTRY_IMAGE}:${LOCAL_VERSION}-dev" \
#      --manifest apqc-php-${LOCAL_VERSION}-dev \
#      --arch arm64 \
#      --file $DIRECTORY/Dockerfile.dev \
#      $DIRECTORY
#exit
echo Pushing images
buildah manifest push --all \
     apqc-php-${LOCAL_VERSION} \
    "docker://${REGISTRY}/${USER}/${IMAGE_NAME}:${IMAGE_TAG}"
buildah manifest push --all \
     apqc-php-${LOCAL_VERSION}-dev \
    "docker://${REGISTRY}/${USER}/${IMAGE_NAME}:${IMAGE_TAG}"
