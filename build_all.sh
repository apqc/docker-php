#!/bin/sh

export STORAGE_DRIVER=vfs
export BUILDAH_LAYERS=true

###############################################################################
###############################################################################
###############################################################################
DIRECTORY="8.1/alpine3.20/fpm"
PHP_VERSION="8.1"
VARIANT="alpine3.20"
CI_REGISTRY_IMAGE=${CI_REGISTRY_IMAGE:-registry.gitlab.com/apqc/docker-php}
LOCAL_VERSION="${PHP_VERSION}-fpm-${VARIANT}"

echo Building apqc/php:${LOCAL_VERSION}
buildah manifest create apqc-php-${LOCAL_VERSION}
buildah bud \
      --tag "${CI_REGISTRY_IMAGE}:${LOCAL_VERSION}" \
      --manifest apqc-php-${LOCAL_VERSION} \
      --arch amd64 \
      --file $DIRECTORY/Dockerfile \
      $DIRECTORY
#buildah bud \
#      --tag "${CI_REGISTRY_IMAGE}:${LOCAL_VERSION}" \
#      --manifest apqc-php-${LOCAL_VERSION} \
#      --arch arm64 \
#      --file $DIRECTORY/Dockerfile \
#      $DIRECTORY
echo Building apqc/php:${LOCAL_VERSION}-dev
buildah manifest create apqc-php-${LOCAL_VERSION}-dev
buildah bud \
      --tag "${CI_REGISTRY_IMAGE}:${LOCAL_VERSION}-dev" \
      --manifest apqc-php-${LOCAL_VERSION}-dev \
      --arch amd64 \
      --file $DIRECTORY/Dockerfile.dev \
      $DIRECTORY
#buildah bud \
#      --tag "${CI_REGISTRY_IMAGE}:${LOCAL_VERSION}-dev" \
#      --manifest apqc-php-${LOCAL_VERSION}-dev \
#      --arch arm64 \
#      --file $DIRECTORY/Dockerfile.dev \
#      $DIRECTORY
#exit
echo Pushing images
buildah manifest push --all \
     apqc-php-${LOCAL_VERSION} \
    "docker://${REGISTRY}/${USER}/${IMAGE_NAME}:${IMAGE_TAG}"
buildah manifest push --all \
     apqc-php-${LOCAL_VERSION}-dev \
    "docker://${REGISTRY}/${USER}/${IMAGE_NAME}:${IMAGE_TAG}"
###############################################################################
###############################################################################
###############################################################################
DIRECTORY="8.1/alpine3.21/fpm"
PHP_VERSION="8.1"
VARIANT="alpine3.21"
CI_REGISTRY_IMAGE=${CI_REGISTRY_IMAGE:-registry.gitlab.com/apqc/docker-php}
LOCAL_VERSION="${PHP_VERSION}-fpm-${VARIANT}"

echo Building apqc/php:${LOCAL_VERSION}
buildah manifest create apqc-php-${LOCAL_VERSION}
buildah bud \
      --tag "${CI_REGISTRY_IMAGE}:${LOCAL_VERSION}" \
      --manifest apqc-php-${LOCAL_VERSION} \
      --arch amd64 \
      --file $DIRECTORY/Dockerfile \
      $DIRECTORY
#buildah bud \
#      --tag "${CI_REGISTRY_IMAGE}:${LOCAL_VERSION}" \
#      --manifest apqc-php-${LOCAL_VERSION} \
#      --arch arm64 \
#      --file $DIRECTORY/Dockerfile \
#      $DIRECTORY
echo Building apqc/php:${LOCAL_VERSION}-dev
buildah manifest create apqc-php-${LOCAL_VERSION}-dev
buildah bud \
      --tag "${CI_REGISTRY_IMAGE}:${LOCAL_VERSION}-dev" \
      --manifest apqc-php-${LOCAL_VERSION}-dev \
      --arch amd64 \
      --file $DIRECTORY/Dockerfile.dev \
      $DIRECTORY
#buildah bud \
#      --tag "${CI_REGISTRY_IMAGE}:${LOCAL_VERSION}-dev" \
#      --manifest apqc-php-${LOCAL_VERSION}-dev \
#      --arch arm64 \
#      --file $DIRECTORY/Dockerfile.dev \
#      $DIRECTORY
#exit
echo Pushing images
buildah manifest push --all \
     apqc-php-${LOCAL_VERSION} \
    "docker://${REGISTRY}/${USER}/${IMAGE_NAME}:${IMAGE_TAG}"
buildah manifest push --all \
     apqc-php-${LOCAL_VERSION}-dev \
    "docker://${REGISTRY}/${USER}/${IMAGE_NAME}:${IMAGE_TAG}"
###############################################################################
###############################################################################
###############################################################################
DIRECTORY="8.2/alpine3.20/fpm"
PHP_VERSION="8.2"
VARIANT="alpine3.20"
CI_REGISTRY_IMAGE=${CI_REGISTRY_IMAGE:-registry.gitlab.com/apqc/docker-php}
LOCAL_VERSION="${PHP_VERSION}-fpm-${VARIANT}"

echo Building apqc/php:${LOCAL_VERSION}
buildah manifest create apqc-php-${LOCAL_VERSION}
buildah bud \
      --tag "${CI_REGISTRY_IMAGE}:${LOCAL_VERSION}" \
      --manifest apqc-php-${LOCAL_VERSION} \
      --arch amd64 \
      --file $DIRECTORY/Dockerfile \
      $DIRECTORY
#buildah bud \
#      --tag "${CI_REGISTRY_IMAGE}:${LOCAL_VERSION}" \
#      --manifest apqc-php-${LOCAL_VERSION} \
#      --arch arm64 \
#      --file $DIRECTORY/Dockerfile \
#      $DIRECTORY
echo Building apqc/php:${LOCAL_VERSION}-dev
buildah manifest create apqc-php-${LOCAL_VERSION}-dev
buildah bud \
      --tag "${CI_REGISTRY_IMAGE}:${LOCAL_VERSION}-dev" \
      --manifest apqc-php-${LOCAL_VERSION}-dev \
      --arch amd64 \
      --file $DIRECTORY/Dockerfile.dev \
      $DIRECTORY
#buildah bud \
#      --tag "${CI_REGISTRY_IMAGE}:${LOCAL_VERSION}-dev" \
#      --manifest apqc-php-${LOCAL_VERSION}-dev \
#      --arch arm64 \
#      --file $DIRECTORY/Dockerfile.dev \
#      $DIRECTORY
#exit
echo Pushing images
buildah manifest push --all \
     apqc-php-${LOCAL_VERSION} \
    "docker://${REGISTRY}/${USER}/${IMAGE_NAME}:${IMAGE_TAG}"
buildah manifest push --all \
     apqc-php-${LOCAL_VERSION}-dev \
    "docker://${REGISTRY}/${USER}/${IMAGE_NAME}:${IMAGE_TAG}"
###############################################################################
###############################################################################
###############################################################################
DIRECTORY="8.2/alpine3.21/fpm"
PHP_VERSION="8.2"
VARIANT="alpine3.21"
CI_REGISTRY_IMAGE=${CI_REGISTRY_IMAGE:-registry.gitlab.com/apqc/docker-php}
LOCAL_VERSION="${PHP_VERSION}-fpm-${VARIANT}"

echo Building apqc/php:${LOCAL_VERSION}
buildah manifest create apqc-php-${LOCAL_VERSION}
buildah bud \
      --tag "${CI_REGISTRY_IMAGE}:${LOCAL_VERSION}" \
      --manifest apqc-php-${LOCAL_VERSION} \
      --arch amd64 \
      --file $DIRECTORY/Dockerfile \
      $DIRECTORY
#buildah bud \
#      --tag "${CI_REGISTRY_IMAGE}:${LOCAL_VERSION}" \
#      --manifest apqc-php-${LOCAL_VERSION} \
#      --arch arm64 \
#      --file $DIRECTORY/Dockerfile \
#      $DIRECTORY
echo Building apqc/php:${LOCAL_VERSION}-dev
buildah manifest create apqc-php-${LOCAL_VERSION}-dev
buildah bud \
      --tag "${CI_REGISTRY_IMAGE}:${LOCAL_VERSION}-dev" \
      --manifest apqc-php-${LOCAL_VERSION}-dev \
      --arch amd64 \
      --file $DIRECTORY/Dockerfile.dev \
      $DIRECTORY
#buildah bud \
#      --tag "${CI_REGISTRY_IMAGE}:${LOCAL_VERSION}-dev" \
#      --manifest apqc-php-${LOCAL_VERSION}-dev \
#      --arch arm64 \
#      --file $DIRECTORY/Dockerfile.dev \
#      $DIRECTORY
#exit
echo Pushing images
buildah manifest push --all \
     apqc-php-${LOCAL_VERSION} \
    "docker://${REGISTRY}/${USER}/${IMAGE_NAME}:${IMAGE_TAG}"
buildah manifest push --all \
     apqc-php-${LOCAL_VERSION}-dev \
    "docker://${REGISTRY}/${USER}/${IMAGE_NAME}:${IMAGE_TAG}"
###############################################################################
###############################################################################
###############################################################################
DIRECTORY="8.3/alpine3.20/fpm"
PHP_VERSION="8.3"
VARIANT="alpine3.20"
CI_REGISTRY_IMAGE=${CI_REGISTRY_IMAGE:-registry.gitlab.com/apqc/docker-php}
LOCAL_VERSION="${PHP_VERSION}-fpm-${VARIANT}"

echo Building apqc/php:${LOCAL_VERSION}
buildah manifest create apqc-php-${LOCAL_VERSION}
buildah bud \
      --tag "${CI_REGISTRY_IMAGE}:${LOCAL_VERSION}" \
      --manifest apqc-php-${LOCAL_VERSION} \
      --arch amd64 \
      --file $DIRECTORY/Dockerfile \
      $DIRECTORY
#buildah bud \
#      --tag "${CI_REGISTRY_IMAGE}:${LOCAL_VERSION}" \
#      --manifest apqc-php-${LOCAL_VERSION} \
#      --arch arm64 \
#      --file $DIRECTORY/Dockerfile \
#      $DIRECTORY
echo Building apqc/php:${LOCAL_VERSION}-dev
buildah manifest create apqc-php-${LOCAL_VERSION}-dev
buildah bud \
      --tag "${CI_REGISTRY_IMAGE}:${LOCAL_VERSION}-dev" \
      --manifest apqc-php-${LOCAL_VERSION}-dev \
      --arch amd64 \
      --file $DIRECTORY/Dockerfile.dev \
      $DIRECTORY
#buildah bud \
#      --tag "${CI_REGISTRY_IMAGE}:${LOCAL_VERSION}-dev" \
#      --manifest apqc-php-${LOCAL_VERSION}-dev \
#      --arch arm64 \
#      --file $DIRECTORY/Dockerfile.dev \
#      $DIRECTORY
#exit
echo Pushing images
buildah manifest push --all \
     apqc-php-${LOCAL_VERSION} \
    "docker://${REGISTRY}/${USER}/${IMAGE_NAME}:${IMAGE_TAG}"
buildah manifest push --all \
     apqc-php-${LOCAL_VERSION}-dev \
    "docker://${REGISTRY}/${USER}/${IMAGE_NAME}:${IMAGE_TAG}"
###############################################################################
###############################################################################
###############################################################################
DIRECTORY="8.3/alpine3.21/fpm"
PHP_VERSION="8.3"
VARIANT="alpine3.21"
CI_REGISTRY_IMAGE=${CI_REGISTRY_IMAGE:-registry.gitlab.com/apqc/docker-php}
LOCAL_VERSION="${PHP_VERSION}-fpm-${VARIANT}"

echo Building apqc/php:${LOCAL_VERSION}
buildah manifest create apqc-php-${LOCAL_VERSION}
buildah bud \
      --tag "${CI_REGISTRY_IMAGE}:${LOCAL_VERSION}" \
      --manifest apqc-php-${LOCAL_VERSION} \
      --arch amd64 \
      --file $DIRECTORY/Dockerfile \
      $DIRECTORY
#buildah bud \
#      --tag "${CI_REGISTRY_IMAGE}:${LOCAL_VERSION}" \
#      --manifest apqc-php-${LOCAL_VERSION} \
#      --arch arm64 \
#      --file $DIRECTORY/Dockerfile \
#      $DIRECTORY
echo Building apqc/php:${LOCAL_VERSION}-dev
buildah manifest create apqc-php-${LOCAL_VERSION}-dev
buildah bud \
      --tag "${CI_REGISTRY_IMAGE}:${LOCAL_VERSION}-dev" \
      --manifest apqc-php-${LOCAL_VERSION}-dev \
      --arch amd64 \
      --file $DIRECTORY/Dockerfile.dev \
      $DIRECTORY
#buildah bud \
#      --tag "${CI_REGISTRY_IMAGE}:${LOCAL_VERSION}-dev" \
#      --manifest apqc-php-${LOCAL_VERSION}-dev \
#      --arch arm64 \
#      --file $DIRECTORY/Dockerfile.dev \
#      $DIRECTORY
#exit
echo Pushing images
buildah manifest push --all \
     apqc-php-${LOCAL_VERSION} \
    "docker://${REGISTRY}/${USER}/${IMAGE_NAME}:${IMAGE_TAG}"
buildah manifest push --all \
     apqc-php-${LOCAL_VERSION}-dev \
    "docker://${REGISTRY}/${USER}/${IMAGE_NAME}:${IMAGE_TAG}"
###############################################################################
###############################################################################
###############################################################################
DIRECTORY="8.4/alpine3.20/fpm"
PHP_VERSION="8.4"
VARIANT="alpine3.20"
CI_REGISTRY_IMAGE=${CI_REGISTRY_IMAGE:-registry.gitlab.com/apqc/docker-php}
LOCAL_VERSION="${PHP_VERSION}-fpm-${VARIANT}"

echo Building apqc/php:${LOCAL_VERSION}
buildah manifest create apqc-php-${LOCAL_VERSION}
buildah bud \
      --tag "${CI_REGISTRY_IMAGE}:${LOCAL_VERSION}" \
      --manifest apqc-php-${LOCAL_VERSION} \
      --arch amd64 \
      --file $DIRECTORY/Dockerfile \
      $DIRECTORY
#buildah bud \
#      --tag "${CI_REGISTRY_IMAGE}:${LOCAL_VERSION}" \
#      --manifest apqc-php-${LOCAL_VERSION} \
#      --arch arm64 \
#      --file $DIRECTORY/Dockerfile \
#      $DIRECTORY
echo Building apqc/php:${LOCAL_VERSION}-dev
buildah manifest create apqc-php-${LOCAL_VERSION}-dev
buildah bud \
      --tag "${CI_REGISTRY_IMAGE}:${LOCAL_VERSION}-dev" \
      --manifest apqc-php-${LOCAL_VERSION}-dev \
      --arch amd64 \
      --file $DIRECTORY/Dockerfile.dev \
      $DIRECTORY
#buildah bud \
#      --tag "${CI_REGISTRY_IMAGE}:${LOCAL_VERSION}-dev" \
#      --manifest apqc-php-${LOCAL_VERSION}-dev \
#      --arch arm64 \
#      --file $DIRECTORY/Dockerfile.dev \
#      $DIRECTORY
#exit
echo Pushing images
buildah manifest push --all \
     apqc-php-${LOCAL_VERSION} \
    "docker://${REGISTRY}/${USER}/${IMAGE_NAME}:${IMAGE_TAG}"
buildah manifest push --all \
     apqc-php-${LOCAL_VERSION}-dev \
    "docker://${REGISTRY}/${USER}/${IMAGE_NAME}:${IMAGE_TAG}"
###############################################################################
###############################################################################
###############################################################################
DIRECTORY="8.4/alpine3.21/fpm"
PHP_VERSION="8.4"
VARIANT="alpine3.21"
CI_REGISTRY_IMAGE=${CI_REGISTRY_IMAGE:-registry.gitlab.com/apqc/docker-php}
LOCAL_VERSION="${PHP_VERSION}-fpm-${VARIANT}"

echo Building apqc/php:${LOCAL_VERSION}
buildah manifest create apqc-php-${LOCAL_VERSION}
buildah bud \
      --tag "${CI_REGISTRY_IMAGE}:${LOCAL_VERSION}" \
      --manifest apqc-php-${LOCAL_VERSION} \
      --arch amd64 \
      --file $DIRECTORY/Dockerfile \
      $DIRECTORY
#buildah bud \
#      --tag "${CI_REGISTRY_IMAGE}:${LOCAL_VERSION}" \
#      --manifest apqc-php-${LOCAL_VERSION} \
#      --arch arm64 \
#      --file $DIRECTORY/Dockerfile \
#      $DIRECTORY
echo Building apqc/php:${LOCAL_VERSION}-dev
buildah manifest create apqc-php-${LOCAL_VERSION}-dev
buildah bud \
      --tag "${CI_REGISTRY_IMAGE}:${LOCAL_VERSION}-dev" \
      --manifest apqc-php-${LOCAL_VERSION}-dev \
      --arch amd64 \
      --file $DIRECTORY/Dockerfile.dev \
      $DIRECTORY
#buildah bud \
#      --tag "${CI_REGISTRY_IMAGE}:${LOCAL_VERSION}-dev" \
#      --manifest apqc-php-${LOCAL_VERSION}-dev \
#      --arch arm64 \
#      --file $DIRECTORY/Dockerfile.dev \
#      $DIRECTORY
#exit
echo Pushing images
buildah manifest push --all \
     apqc-php-${LOCAL_VERSION} \
    "docker://${REGISTRY}/${USER}/${IMAGE_NAME}:${IMAGE_TAG}"
buildah manifest push --all \
     apqc-php-${LOCAL_VERSION}-dev \
    "docker://${REGISTRY}/${USER}/${IMAGE_NAME}:${IMAGE_TAG}"
